﻿using System;

namespace DicomLense.WpfToolkit
{
    internal interface IViewModelFactory
    {
        ViewModelBase Create(Type viewModelType);
    }
}
