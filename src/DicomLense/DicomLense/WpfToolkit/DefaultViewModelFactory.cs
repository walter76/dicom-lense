﻿using System;

namespace DicomLense.WpfToolkit
{
    internal class DefaultViewModelFactory : IViewModelFactory
    {
        public ViewModelBase Create(Type viewModelType)
        {
            return Activator.CreateInstance(viewModelType) as ViewModelBase;
        }
    }
}
